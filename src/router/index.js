import Vue from 'vue'
import VueRouter from 'vue-router'
import About from '../views/About'
import Work from '../views/Work'
import Certificate from '../views/Certificate'

Vue.use(VueRouter)

const routes = [{
    path: '/',
    name: 'About',
    meta: {
      navbar: true
    },
    component: About
  },
  {
    path: '/work',
    name: 'Work',
    meta: {
      navbar: true
    },
    component: Work
  },
  {
    path: '/certificate',
    name: 'Certificate',
    meta: {
      navbar: true
    },
    component: Certificate
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router